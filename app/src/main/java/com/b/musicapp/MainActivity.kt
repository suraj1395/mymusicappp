package com.b.musicapp

import SongsAdapter
import WebService
import android.graphics.Color
import android.graphics.PorterDuff
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.b.medcords.Webservices.ApiUtils
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import java.util.HashMap

class MainActivity : AppCompatActivity() {

    private var recycleOrder: RecyclerView?=null
    var adapter: SongsAdapter?=null
    //    var serachSong:EditText?=null
    var btnSearch:ImageView?=null
    private var artName: String? = null
    private var tvMsg:TextView?=null
    private var tvLoading:TextView?=null

    var artistName:String =""
    val songList = ArrayList<HashMap<String, String>>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initControl()
    }

    private fun initControl() {

        btnSearch =findViewById<ImageView>(R.id.btnSearch) as ImageView

        tvMsg =findViewById(R.id.tvMsg) as TextView

        tvLoading =findViewById(R.id.tvLoading) as TextView

        recycleOrder =findViewById(R.id.recycleSong) as RecyclerView
        recycleOrder?.setHasFixedSize(true)
        recycleOrder?.setLayoutManager(GridLayoutManager(this,2))
        ProgressBar.getIndeterminateDrawable().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);

        btnSearch!!.setOnClickListener(){
            // getSongs(artistName)
            if (isValidate) {
                var  artName = serachSong.text.toString().trim()
                //  Toast.makeText(this@MainActivity, artName, Toast.LENGTH_SHORT).show()


                tvMsg!!.visibility=View.GONE
                tvLoading!!.visibility=View.VISIBLE


                recycleOrder!!.visibility=View.VISIBLE


                getNewSongs(artName)
                ProgressBar.visibility=View.VISIBLE

            }
        }
    }


    private fun getNewSongs(artistName: String) {
        val mAPIService: WebService = ApiUtils.aPIService
        val call= mAPIService.getSongs(artistName)
        call?.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(callback: Call<ResponseBody?>?, response: Response<ResponseBody?>) {
                val result:String?= response.body()?.string()

                ProgressBar.visibility=View.VISIBLE

                if(response.isSuccessful){
                    if (result != null) {
                        songList.clear()
                        try {
                            val jsonObject = JSONObject(result)
                            tvLoading!!.visibility=View.GONE
                            ProgressBar.visibility=View.GONE


                            val jsonArray: JSONArray = jsonObject.getJSONArray("results")
                            //mTeamName = arrayOfNulls<String?>(jsonArray.length())
                            //    teamName=jsonArray.length()
                            for (i in 0 until jsonArray.length()) {
                                val jObjP: JSONObject = jsonArray.getJSONObject(i)

                                val map = HashMap<String, String>()
                                map["artistName"] = jObjP.getString("artistName")
                                map["trackName"] = jObjP.getString("trackName")
                                map["artworkUrl60"] = jObjP.getString("artworkUrl60")
                                map["previewUrl"] = jObjP.getString("previewUrl")
                                songList.add(map)
                            }


                            adapter = applicationContext?.let { SongsAdapter(this@MainActivity, songList) }
                            recycleOrder!!.adapter = adapter
                            adapter!!.notifyDataSetChanged()



                        } catch (e: Exception) { e.printStackTrace() }

                    }
                }else{
                    Toast.makeText(this@MainActivity,"Went something wrong!!", Toast.LENGTH_SHORT).show()
                }
            }
            override fun onFailure(call: Call<ResponseBody?>?, t: Throwable?) {
                tvLoading!!.visibility=View.VISIBLE


            }
        })
    }


    private val isValidate: Boolean
        private get() {
            if (serachSong!!.text.toString().isEmpty()) {
                Toast.makeText(this, "Please enter ArtistName or Album name.", Toast.LENGTH_SHORT)
                    .show()
                return false
            }
            return true
        }


}