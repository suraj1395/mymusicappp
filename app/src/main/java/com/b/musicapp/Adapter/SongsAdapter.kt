
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.b.musicapp.R
import com.squareup.picasso.Picasso
import java.util.*

 class SongsAdapter(private val mCtx: FragmentActivity?, private val mList: ArrayList<HashMap<String, String>>) : Adapter<SongsAdapter.ProductViewHolder?>()
      {


     var songList = ArrayList<song_list>()

     override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
         val view: View? = mCtx?.let { LayoutInflater.from(it).inflate(R.layout.order_item, parent, false) }
         return ProductViewHolder(view!!)
     }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val `object` = mList[position]
        holder.tvSongName.text = `object`.get("trackName")

        Picasso.with(mCtx).load(`object`.get("artworkUrl60")).into(holder.imgBanner)

    }

    override fun getItemCount(): Int {
        return mList.size
    }

     inner class ProductViewHolder(itemView: View) : ViewHolder(itemView) {
        var tvSongName: TextView
         var imgBanner: ImageView
         var lliew:LinearLayout

        init {
            tvSongName = itemView.findViewById(R.id.tvSongName)
            imgBanner = itemView.findViewById(R.id.imgBanner)
            lliew = itemView.findViewById(R.id.lliew)

        }
    }

}
