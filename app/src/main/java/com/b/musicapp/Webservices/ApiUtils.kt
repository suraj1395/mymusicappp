package com.b.medcords.Webservices

import WebService

object ApiUtils {
    const val BASE_URL = "https://itunes.apple.com/"

   // https://itunes.apple.com/search?term=ajayatul
    //const val BASE_URL = "https://bulbandkey.com/endpoint/mobile/"

    val aPIService: WebService
        get() = RetrofitClient.getClient(BASE_URL)!!.create(WebService::class.java)
}